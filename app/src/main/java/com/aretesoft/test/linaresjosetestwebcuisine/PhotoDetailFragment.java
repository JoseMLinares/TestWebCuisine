package com.aretesoft.test.linaresjosetestwebcuisine;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aretesoft.test.linaresjosetestwebcuisine.PhotoStruct.Photo;
import com.aretesoft.test.linaresjosetestwebcuisine.dummy.DummyContent;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * A fragment representing a single Photo detail screen.
 * This fragment is either contained in a {@link PhotoListActivity}
 * in two-pane mode (on tablets) or a {@link PhotoDetailActivity}
 * on handsets.
 */
public class PhotoDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    public Photo photoNasa;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PhotoDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            photoNasa = PhotoListActivity.getPhotoList().get(getArguments().getInt(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(photoNasa.getId().toString());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.photo_detail, container, false);

        final ImageView imagePhotoNasa = (ImageView) rootView.findViewById(R.id.imagePhotoNasaDetail);

        new AsyncTask<String, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(String... strings) {
                Bitmap bitmap = null;

                try {
                    bitmap = BitmapFactory.decodeStream((InputStream) new URL(strings[0]).getContent());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("ERROR","Error Exception: "+e);
                }

                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if(isCancelled())
                    bitmap = null;
                if(bitmap != null){
                    imagePhotoNasa.setImageBitmap(bitmap);
                }
            }
        }.execute(photoNasa.getImg_src());

        return rootView;
    }
}
