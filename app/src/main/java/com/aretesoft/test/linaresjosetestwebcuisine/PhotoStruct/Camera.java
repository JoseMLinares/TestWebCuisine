package com.aretesoft.test.linaresjosetestwebcuisine.PhotoStruct;

import com.google.gson.annotations.SerializedName;

/**
 * Created by JoseM on 28/10/16.
 */

public class Camera {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("rover_id")
    private Integer rover_id;

    @SerializedName("full_name")
    private String full_name;

    public Camera(Integer id, String name, Integer rover_id, String full_name) {
        this.id = id;
        this.name = name;
        this.rover_id = rover_id;
        this.full_name = full_name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRover_id() {
        return rover_id;
    }

    public void setRover_id(Integer rover_id) {
        this.rover_id = rover_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}
