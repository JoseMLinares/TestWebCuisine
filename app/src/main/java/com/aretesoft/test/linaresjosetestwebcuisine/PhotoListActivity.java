package com.aretesoft.test.linaresjosetestwebcuisine;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.aretesoft.test.linaresjosetestwebcuisine.PhotoStruct.NasaResponse;
import com.aretesoft.test.linaresjosetestwebcuisine.PhotoStruct.Photo;
import com.aretesoft.test.linaresjosetestwebcuisine.Services.WebServicesClient;
import com.aretesoft.test.linaresjosetestwebcuisine.Services.WebServicesInterface;
import com.aretesoft.test.linaresjosetestwebcuisine.dummy.DummyContent;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An activity representing a list of Photos. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link PhotoDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class PhotoListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    static List<Photo> photoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();


        WebServicesInterface servicesInterface = WebServicesClient.getClient().create(WebServicesInterface.class);

        Call<NasaResponse> callJsonPhotos = servicesInterface.getPhotosNASA();

        callJsonPhotos.enqueue(new Callback<NasaResponse>() {
            @Override
            public void onResponse(Call<NasaResponse> call, Response<NasaResponse> response) {
                photoList=response.body().getPhotos();

                View recyclerView = findViewById(R.id.photo_list);
                assert recyclerView != null;
                setupRecyclerView((RecyclerView) recyclerView);
            }

            @Override
            public void onFailure(Call<NasaResponse> call, Throwable t) {
                Log.e("ERROR", "Error: "+t);
            }
        });


        if (findViewById(R.id.photo_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(photoList));
    }

    public static List<Photo> getPhotoList(){
        return photoList;
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.PhotoHolder> {

        private List<Photo> photoList;

        public SimpleItemRecyclerViewAdapter(List<Photo> photoList) {
            this.photoList=photoList;
        }

        @Override
        public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.photo_list_content, parent, false);
            return new PhotoHolder(view);
        }

        @Override
        public void onBindViewHolder(final PhotoHolder holder, final int position) {
            holder.photo=photoList.get(position);
            holder.textEarthDate.setText(holder.photo.getEarth_date());
            holder.textCameraFullName.setText(holder.photo.getCamera().getFull_name());


            if(position%2==0){
                holder.layout.setBackgroundColor(Color.GRAY);
            }


            new AsyncTask<String, Void, Bitmap>(){
                @Override
                protected Bitmap doInBackground(String... strings) {
                    Bitmap bitmap = null;

                    try {
                        bitmap = BitmapFactory.decodeStream((InputStream) new URL(strings[0]).getContent());
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("ERROR","Error Exception: "+e);
                    }

                    return bitmap;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    if(isCancelled())
                        bitmap = null;
                    if(bitmap != null){
                        holder.imagePhotoNasa.setImageBitmap(bitmap);
                    }
                }
            }.execute(photoList.get(position).getImg_src());

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putInt(PhotoDetailFragment.ARG_ITEM_ID, position);
                        PhotoDetailFragment fragment = new PhotoDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.photo_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, PhotoDetailActivity.class);
                        intent.putExtra(PhotoDetailFragment.ARG_ITEM_ID, position);

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return photoList.size();
        }

        public class PhotoHolder extends RecyclerView.ViewHolder{
            View view;
            Photo photo;
            TextView textEarthDate;
            TextView textCameraFullName;
            ImageView imagePhotoNasa;
            RelativeLayout layout;

            public PhotoHolder(View itemView) {
                super(itemView);
                this.view = itemView;

                textEarthDate = (TextView) itemView.findViewById(R.id.textEarthDate);
                textCameraFullName = (TextView) itemView.findViewById(R.id.textCameraFullName);
                imagePhotoNasa = (ImageView) itemView.findViewById(R.id.imagePhotoNasa);
                layout = (RelativeLayout) itemView.findViewById(R.id.layoutList);
            }
        }


    }
}
