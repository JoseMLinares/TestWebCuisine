package com.aretesoft.test.linaresjosetestwebcuisine.Services;

import com.aretesoft.test.linaresjosetestwebcuisine.PhotoStruct.NasaResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by JoseM on 28/10/16.
 */

public interface WebServicesInterface {

    @GET("photos?sol=1000&api_key=DEMO_KEY")
    Call<NasaResponse> getPhotosNASA();
}
