package com.aretesoft.test.linaresjosetestwebcuisine.PhotoStruct;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by JoseM on 28/10/16.
 */

public class NasaResponse {

    @SerializedName("photos")
    private ArrayList<Photo> photos = new ArrayList<Photo>();

    public NasaResponse(ArrayList<Photo> photos) {
        this.photos = photos;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }
}
