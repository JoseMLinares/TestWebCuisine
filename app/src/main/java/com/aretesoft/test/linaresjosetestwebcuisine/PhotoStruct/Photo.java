package com.aretesoft.test.linaresjosetestwebcuisine.PhotoStruct;

import com.google.gson.annotations.SerializedName;

/**
 * Created by JoseM on 28/10/16.
 */

public class Photo {

    @SerializedName("id")
    private Double id;

    @SerializedName("sol")
    private Integer sol;

    @SerializedName("camera")
    private Camera camera;

    @SerializedName("img_src")
    private String img_src;

    @SerializedName("earth_date")
    private String earth_date;

    @SerializedName("rover")
    private Rover rover;

    public Photo(Double id, Integer sol, Camera camera, String img_src, String earth_date, Rover rover) {
        this.id = id;
        this.sol = sol;
        this.camera = camera;
        this.img_src = img_src;
        this.earth_date = earth_date;
        this.rover = rover;
    }

    public Double getId() {
        return id;
    }

    public void setId(Double id) {
        this.id = id;
    }

    public Integer getSol() {
        return sol;
    }

    public void setSol(Integer sol) {
        this.sol = sol;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public String getImg_src() {
        return img_src;
    }

    public void setImg_src(String img_src) {
        this.img_src = img_src;
    }

    public String getEarth_date() {
        return earth_date;
    }

    public void setEarth_date(String earth_date) {
        this.earth_date = earth_date;
    }

    public Rover getRover() {
        return rover;
    }

    public void setRover(Rover rover) {
        this.rover = rover;
    }
}
