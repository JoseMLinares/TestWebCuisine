package com.aretesoft.test.linaresjosetestwebcuisine.PhotoStruct;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by JoseM on 28/10/16.
 */

public class Rover {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("landing_date")
    private String landing_date;

    @SerializedName("launch_date")
    private String launch_date;

    @SerializedName("status")
    private String status;

    @SerializedName("max_sol")
    private Integer max_sol;

    @SerializedName("max_date")
    private String max_date;

    @SerializedName("total_photos")
    private Double total_photos;

    @SerializedName("cameras")
    private ArrayList<RoverCamera> cameras = new ArrayList<RoverCamera>();

    public Rover(Integer id, String name, String landing_date, String launch_date, String status, Integer max_sol, String max_date, Double total_photos, ArrayList<RoverCamera> cameras) {
        this.id = id;
        this.name = name;
        this.landing_date = landing_date;
        this.launch_date = launch_date;
        this.status = status;
        this.max_sol = max_sol;
        this.max_date = max_date;
        this.total_photos = total_photos;
        this.cameras = cameras;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanding_date() {
        return landing_date;
    }

    public void setLanding_date(String landing_date) {
        this.landing_date = landing_date;
    }

    public String getLaunch_date() {
        return launch_date;
    }

    public void setLaunch_date(String launch_date) {
        this.launch_date = launch_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getMax_sol() {
        return max_sol;
    }

    public void setMax_sol(Integer max_sol) {
        this.max_sol = max_sol;
    }

    public String getMax_date() {
        return max_date;
    }

    public void setMax_date(String max_date) {
        this.max_date = max_date;
    }

    public Double getTotal_photos() {
        return total_photos;
    }

    public void setTotal_photos(Double total_photos) {
        this.total_photos = total_photos;
    }

    public ArrayList<RoverCamera> getCameras() {
        return cameras;
    }

    public void setCameras(ArrayList<RoverCamera> cameras) {
        this.cameras = cameras;
    }
}
