package com.aretesoft.test.linaresjosetestwebcuisine.Services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by JoseM on 28/10/16.
 */

public class WebServicesClient {

    public static final String URL = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient(){
        if(retrofit==null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
